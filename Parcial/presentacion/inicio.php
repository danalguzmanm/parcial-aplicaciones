<div class="container mt-5 mb-5">
    <div class="card">
        <div class="card-header text-white bg-info">
            <h4>Bienvenido </h4>
        </div>
        <div class="card-body">
            <h5 class="card-title">En nuestra pagina encontrara una gran variedad de productos</h5>
            <div class="row">
                <div class="col-lg-4 ">
                    <div class="card" style="width: 100%;">
                        <img src="img/carro.jpg" class="card-img-top">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card" style="width: 100%;">
                        <img src="img/muñeca.jpg" class="card-img-top">
                    </div>
                </div>

                <div class="col-lg-4">
                    <div class="card" style="width: 100%;">
                        <img src="img/balon.jpg" class="card-img-top">
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>