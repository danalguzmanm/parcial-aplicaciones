<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/JugueteDAO.php";
class Juguete{
    private $id;
    private $nombre;
    private $material;
    private $cantidad;
    private $conexion;
    private $jugueteDAO;
    
    public function getId(){
        return $this -> id;
    }
    
    public function getNombre(){
        return $this -> nombre;
    }
    
    public function getCantidad(){
        return $this -> cantidad;
    }
    
    public function getMaterial(){
        return $this -> material;
    }
        
    public function Juguete($id = "", $nombre = "", $cantidad = "", $material = ""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> material = $material;
        $this -> conexion = new Conexion();
        $this -> jugueteDAO = new JugueteDAO($this -> id, $this -> nombre, $this -> material, $this -> cantidad);
    }
    
    public function insertar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> jugueteDAO -> insertar());        
        $this -> conexion -> cerrar();        
    }
    
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> jugueteDAO -> consultarTodos());
        $juguetes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Juguete($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($juguetes, $p);
        }
        $this -> conexion -> cerrar();        
        return $juguetes;
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> jugueteDAO -> consultarPaginacion($cantidad, $pagina));
        $juguetes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $p = new Juguete($resultado[0], $resultado[1], $resultado[2], $resultado[3]);
            array_push($juguetes, $p);
        }
        $this -> conexion -> cerrar();
        return $juguetes;
    }
    
    public function consultarCantidad(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> jugueteDAO -> consultarCantidad());
        $this -> conexion -> cerrar();
        return $this -> conexion -> extraer()[0];
    }
    
}

?>