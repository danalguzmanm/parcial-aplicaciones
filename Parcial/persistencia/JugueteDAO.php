<?php
class JugueteDAO{
    private $id;
    private $nombre;
    private $material;
    private $cantidad;
       
    public function JugueteDAO($id = "", $nombre = "", $material = "", $cantidad = ""){
        $this -> id = $id;
        $this -> nombre = $nombre;
        $this -> cantidad = $cantidad;
        $this -> material = $material;
    }
       
    public function insertar(){
        return "insert into juguete (nombre, cantidad, material)
                values ('" . $this -> nombre . "', '" . $this -> cantidad . "', '" . $this -> material . "')";
    }
    
    public function consultarTodos(){
        return "select id, nombre, cantidad, material
                from juguete";
    }
    
    public function consultarPaginacion($cantidad, $pagina){
        return "select id, nombre, cantidad, material
                from juguete
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarCantidad(){
        return "select count(id)
                from juguete";
    }
    
}

?>