<?php
$nombre = "";
if(isset($_POST["nombre"])){
    $nombre = $_POST["nombre"];
}
$cantidad = "";
if(isset($_POST["cantidad"])){
    $cantidad = $_POST["cantidad"];
}
$material = "";
if(isset($_POST["material"])){
    $material = $_POST["material"];
}    
if(isset($_POST["crear"])){
    $j = new Juguete("", $nombre, $cantidad, $material);
    $j -> insertar();    
}
?>
<div class="container mt-4">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white">
					<h4>Crear Juguete</h4>
				</div>
              	<div class="card-body" style="text-align: left;">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("Juguete/Crear.php") ?>" method="post">
						<div class="form-group">
							<label>Nombre</label> 
							<input type="text" name="nombre" class="form-control" value="<?php echo $nombre ?>" required>
						</div>
						<div class="form-group">
							<label>Cantidad</label> 
							<input type="number" name="cantidad" class="form-control" min="1" value="<?php echo $cantidad ?>" required>
						</div>
						<div class="form-group">
							<label>Material</label> 
							<input type="text" name="material" class="form-control" value="<?php echo $material ?>" required>
						</div>
						<button type="submit" name="crear" class="btn btn-info">Crear</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>